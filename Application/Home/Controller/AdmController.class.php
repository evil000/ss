<?php
namespace Home\Controller;
use Think\Controller;
class AdmController extends Controller {
    function __construct(){
        parent::__construct();
        //判断SESSION中的ID是否存在
        $this->id = session('id');
        if($this->id < 1){
            return $this->error("请登录帐号",U("Home/Login/index"));
        }
        //判断AID是否有效
        $this->user = D("user")->where(array('id'=>$this->id))->find();
        if(!$this->user){
            return $this->error("无效的帐号",U("Home/Login/index"));
        }
    }
}