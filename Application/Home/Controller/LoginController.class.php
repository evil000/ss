<?php
namespace Home\Controller;
use Think\Controller;
class LoginController extends Controller {
    public function index(){
        $this->display();
        //实例化数据库
        $ss = D("user");

        $do = I("get.do");
        //判断是否进行登录操作
        if($do == 'chk'){
            //获取前端数据
            $user = I("post.user");
            $pass = I("post.pass");
            //数据和数据库里的数据作对比
            $where = array(
                'user' => $user,
                'pass' => $pass,
            );
            $is = $ss->where($where)->find();
       
           // var_dump($is['id']);
   
           //  die();
            //判断帐号密码是否正确
            if (!$is) {
                return $this->error("帐号或密码错误", U("Home/Login/index"));
            }
            session("id", $is['id']);
            return $this->success("登录成功", U("Home/Index/index"));
        }
        }
    }