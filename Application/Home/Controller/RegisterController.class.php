<?php
namespace Home\Controller;
use Think\Controller;
class RegisterController extends Controller {
    public function index(){
        $this->display();
        //实例化数据库
        $ss = D("user");

        /*
         * 这只是用于测试以及演示的一个思路
         */

        if(IS_POST) {
            //获取Register前端发来的数据
            $user = I("post.user");
            $ssid = I("post.ssid");
            $pass = I("post.pass");
            $email = I("post.email");
            $phone = I("post.phone");
            $repass = I("post.repass");
            //验证输入的数据
            $rule = array(
                array('user','','帐号名称已经存在！',0,'unique',1), // 在新增的时候验证name字段是否唯一
                array('user', 'require', "用户名不能为空"),
                array('pass', 'require', "密码不能为空"),
                array('ssid', 'require', "昵称不能为空"),
                array('email', 'require', "邮箱不能为空"),
                array('phone', 'require', "手机号不能为空"),
                array('repass', 'pass', '确认密码不正确', 0, 'confirm'),
            );
            if (!$ss->validate($rule)->create()) {
                return $this->error($ss->getError(), U("Home/Register/index"));
            }
            //数据写入数据库
            $insert = array(
                'user' => $user,
                'ssid' => $ssid,
                'pass' => $pass,
                'email' => $email,
                'phone' => $phone
            );
            $is = $ss->add($insert);
            //判断写入数据库是否成功
            if($is){
                return $this->success('操作成功',U("/Home/Index/index"));
            }else{
                return $this->error('操作失败',U("/Home/Register/index"));
            }
        }
        }
    }