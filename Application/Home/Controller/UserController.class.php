<?php
namespace Home\Controller;
use Think\Controller;
class UserController extends Controller {
   
    public function index(){
        $userId=I("get.userId");
        //实例化数据库
        $ss = D("user");
        
        // 修改数据后显示用户信息
        if($_GET['display']){
            $this->display=$_GET['display'];
        }

        // echo $userId;die();
        // $userInfo=json_encode($ss->where(array('bid'=>$userId))->select());
        $userInfo=$ss->where(array('bid'=>$userId))->select();
        // var_dump($userInfo[0]);
        // die();
        
        $this->userInfo=$userInfo[0];//用户ID

        $url='http://'.$_SERVER['HTTP_HOST'].substr($PHP_SELF,0,strrpos($PHP_SELF,'/')+1);//http://localhost:8080

                                                   // http://localhost:8080/SaySayingMY/clientFile/up/1/1492005732115.jpg
                                                   // 
        $url2=substr($userInfo[0]['headimg'],strrpos($userInfo[0]['headimg'],'SaySayingMY'));    
        $headImgSrc=$url.'/'.$url2;

        // echo $headImgSrc;die();
        $this->headImgSrc=$headImgSrc;//组合过的头像地址
        $this->display();
        //实例化数据库
       
    }

    // 修改头像
     public function setImg(){

        if(IS_POST){

           $file=$_FILES['headImg'];//上传的文件

            if(($file['name'])){
             
                $fileType=strrchr($file['name'],".");

                if($fileType=='.png' || $fileType=='.jpg' || $fileType=='.jpeg'){                  


                         if(is_uploaded_file($file['tmp_name'])){

                             


                              $useName=I("post.userDir");//用户Id
                              $userId=$useName;
                              // echo $useName;die();
                               $srcDir=$file['tmp_name'];

                               
                               //给每个用户创建一个文件夹
                              $use_dir=$_SERVER['DOCUMENT_ROOT'].__ROOT__."/clientFile/up/".$useName;

                              // echo $use_dir;die();

                              if(!file_exists($use_dir)){
                                 $res=mkdir($use_dir);
                                 
                              }  
                          
                               $file_name=$file['name'];
                               $save_name=substr($file_name,strrpos($file_name,"."));
                               $endDir=$use_dir."/".time().rand(1,1000).$save_name;

                           

                               //$endDir=iconv("utf-8","gb2312",$_SERVER['DOCUMENT_ROOT']."/file/up/".$_FILES['myfile']['name']);
                               move_uploaded_file($srcDir,iconv("utf-8","gb2312",$endDir));//头像上传到文件夹
                                
                                // 头像文件夹位置入库
                             

                              $ss=D("user");
                              
                              $data=array(
                                'id'=>$userId,
                                "headimg"=>$endDir
                              );//用户头像地址

                              $res=$ss->save($data);//入库
                              if($res==1){
                                 // $this->redirect("Home/Login/index");
                            

                                  $this->success('头像上传成功', 'Index/User/index');
                               }else{
                                  $this->error('头像上传失败！！', 'Index/User/index');
                               } 

                          
                          }else{
                              $this->error("上传失败","Index/User/index");
                             
                         }


                }else{
                   $this->error("上传头像格式不对","Index/User/index");

                }
            }else{

              $this->error("请选择上传文件","Index/User/index");
            }
        }
    


         die();
            
      
    }

    // 用户退出
    public function Logout(){
        session_unset();
        session_destroy();
        $this->redirect("Home/Login/index");
      
    }

    // 修改用户信息
    public function Edit_info(){
        if(IS_POST){
            // echo "ok";die();
          
           // empty($_POST['user_phone']) || empty($_POST['user_phone']
           
           $phone=trim($_POST['user_phone']);
           $email=trim($_POST['user_email']);
           $sex=intval($_POST['user_sex']);

           $tel_reg='/^(\+?86-?)?(18|15|13)[0-9]{9}$/';
           $email_reg="/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/";

         
               
            // 判断手机号性别正确填写
            if(preg_match($tel_reg,$phone) && preg_match($email_reg,$email)){
                
                 // 组织用户数据
                    $data=array(
                        "id"=>I("post.user_id"),
                        "phone"=>I("post.user_phone"),
                        "email"=>I("post.user_email"),
                        "sex"=>(I("post.user_sex")),
                    );

               

                // 数据入库
               $ss=M("user");
               $res= $ss->save($data);

               if($res==1){
                    $this->success('修改成功', 'Index/User/index');
               }else{
                    $this->success('入库有误', 'Index/User/index');
               }

            }else{
               $this->error('修改失败', 'Index/User/change');
            }
        }
    }

    public function Change(){

     $userId=I("get.id");

      $where=array('id'=>$userId);
       $ss = M("user");
       $userInfo=$ss->where($where)->select();
   
    
     $this->userInfo=$userInfo[0];
    
     $this->display();
    }


    // 删除
    public function Del(){
          $id=$_GET['id'];
          //$where=array('id'=>$id);
          if(M('Blog')->delete($id)){
               M('blog_attr')->where(array('bid'=>$id))->delete();
               $this->success("删除成功");
         }else{
               $this->error("删除失败");
         }
    }
}  