<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="__PUBLIC__/css/bootstrap.min.css">
    <style type="text/css">
        .container{
            margin-top: 4em;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="top navbar-fixed-top text-center">
        <div class="logo">英文logo 用户注册</div>
    </div>
    <form class="form-horizontal" method="post" action="<?php echo U('Home/Register/index') ?>?id=<?php echo $user['id']; ?>">
        <div class="form-group">
            <label for="" class="col-sm-2 col-xs-2 control-label">用户名</label>
            <div class="col-sm-10 col-xs-10">
                <input type="text" class="form-control" name="user" id="" placeholder="用户名/请使用字母数字组合">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 col-xs-2 control-label">昵称</label>
            <div class="col-sm-10 col-xs-10">
                <input type="text" class="form-control" name="ssid" id="" placeholder="昵称">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 col-xs-2 control-label">密码</label>
            <div class="col-sm-10 col-xs-10">
                <input type="password" class="form-control" name="pass" id="" placeholder="密码">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 col-xs-2 control-label">确认密码</label>
            <div class="col-sm-10 col-xs-10">
                <input type="password" class="form-control" name="repass" id="" placeholder="确认密码">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 col-xs-2 control-label">邮箱</label>
            <div class="col-sm-10 col-xs-10">
                <input type="email" class="form-control" name="email" id="" placeholder="邮箱">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 col-xs-2 control-label">手机</label>
            <div class="col-sm-10 col-xs-10">
                <input type="text" class="form-control" name="phone" id="" placeholder="手机">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">注册</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>