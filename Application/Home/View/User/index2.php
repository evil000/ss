<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>个人中心</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="__PUBLIC__/css/bootstrap.min.css"/>
    <script src="__PUBLIC__/js/jquery-1.12.3.js" type="text/javascript" charset="utf-8"></script>
    <script src="__PUBLIC__/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-xs-2 col-md-offset-5 block-center">
           <a href="{:U('/Home/User/Change?',array('id'=>$userInfo['id']))}">头像{$userInfo.username}</a>


        </div>
        
    </div>
     
     <div class="row">
        <div class="col-xs-2 col-md-offset-5 block-center">用户名：{$userInfo.username}</div>       
        <div class="col-xs-2 col-md-offset-5 block-center">性别：
                <if condition="($userInfo.sex eq 0)"> 
                   男
                <else />
                   女
                </if>
        </div>       
        <div class="col-xs-2 col-md-offset-5 block-center">手机：{$userInfo.phone}</div>       
    </div>    
         

</div>
</body>
</html>