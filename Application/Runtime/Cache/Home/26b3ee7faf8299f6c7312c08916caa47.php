<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="/SaySaying/Public/css/bootstrap.min.css">
	<style type="text/css">
	.container{
		margin-top: 10em;
	}
	</style>

	<script type="text/javascript" src="/SaySaying/Public/js/jquery-1.12.3.js"></script>

	<script>

		$().ready(function(){
				// alert("d");
			$("#edit_info").click(function(){
				$("#user_info").addClass("show");
			});
			
		});

	</script>

</head>
<body>
<div class="container">
	 
		
		<!-- 头像 -->
	   <div class="row col-sm-offset-4">
		 	<div class="col-md-8 col-xs-8">
		 		<div class="">
		 			<img class="img-circle" style="width:200px;height=200px" src="/SaySaying/Public/img/djdz-0101.jpg" />
		 		</div>

			</div>	
		</div>
		<br/>
		 <div class="row col-sm-offset-3">
		 	<div class="col-md-8 col-xs-8">
			 	
			 		<a href="#" class="label label-info">头像设置</a>
			 		<a href="#"  class="label label-info">用户名更改</a>
			 		<a href="#" class="label label-info">充值</a>
			 		<a id="edit_info" href="#" class="label label-info">个人信息更改</a>
			 		<a href="#" class="label label-info">退出</a>
		
				
			</div>	
		</div>
		<br/>

		<!-- 个人信息查看修改 -->
		<div id="user_info" class="hide">
			<form action="<?php echo U('/Home/User/Edit_info');?>" method="post">

				<input type="hidden" name="user_id" value="<?php echo ($userInfo["id"]); ?>" />
			   <div class="row col-sm-offset-3 ">
				 	<div class="col-md-7 col-xs-7">
						<span class="label label-default">用户名：</span>
						<input class="form-control col-md-6 col-xs-6" type="text" name="user_name" value="<?php echo ($userInfo["username"]); ?>" disabled="disabled">
					</div>	
				</div>
				<br/>
				<div class="row col-sm-offset-3 ">
					<div class="col-md-7 col-xs-7">
						<span class="label label-default">手机：</span>
						<input  class="form-control col-md-6 col-xs-6" type="text" name="user_phone" value="<?php echo ($userInfo["phone"]); ?>" >
					</div>
				</div>
				<br/>
				<div class="row col-sm-offset-3 ">
					<div class="col-md-7 col-xs-7">
						<span class="label label-default">邮箱：</span>
						<input  class="form-control col-md-6 col-xs-6" type="text" name="user_email" value="<?php echo ($userInfo["email"]); ?>" >
					</div>
				</div>
				<br/>
				<div class="row col-sm-offset-3 ">
					<div class="col-md-7 col-xs-7">
						<span class="label label-default">性别：</span>
						<input  class="form-control col-md-6 col-xs-6" type="text" name="user_sex" value='<?php if(($userInfo["sex"] == 0)): ?>男
				                <?php else: ?>
				                   女<?php endif; ?>'>
				    </div>
				</div>	
				
				<br/>
				<div class="row col-sm-offset-4 ">
					<input type="submit" class="btn btn-primary col-md-4 col-xs-4" value="保存">
				</div>
				
			</form>
		</div>	
</div>
	
	
</body>
</html>