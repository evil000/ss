<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="/SaySayingMY/Public/css/bootstrap.min.css">
	<style type="text/css">
	ul,li{
		list-style-type:none;
	}
	.container{
		margin-top: 10em;
	}
	</style>

	<script type="text/javascript" src="/SaySayingMY/Public/js/jquery-1.12.3.js"></script>

	<script>

		$().ready(function(){
				// alert("d");
		    // 个人信息修改显示
			$("#edit_info").click(function(){
				// $("#list").find("li #user_info").show().siblings("li").hide();
	
				$("#list").find("#user_info").show().siblings().hide();
			});
			 // 修改头像显示
			$("#setImg").click(function(){
				// $("#list").find("#imgArea").addClass("show").siblings().reomveClass("show");
				$("#list").find("#imgArea").show().siblings().hide();
			});

			function changeHeadImg(){

				var headImgSrc=$("headImg").attr("src");

			
			}
			
		});

	</script>

</head>
<body>
<div class="container">
	 
		
		<!-- 头像 -->
	   <div class="row col-sm-offset-4">
		 	<div class="col-md-8 col-xs-8">
		 		<div id="headImg">
		 			<img class="img-circle" style="width:200px;height=200px" src="<?php echo ($headImgSrc); ?>" headImgSrc="<?php echo ($userInfo["headimg"]); ?>"/>
		 		</div>

			</div>	
		</div>
		<br/>

		 <div class="row col-sm-offset-3">
			 	<div class="col-md-8 col-xs-8">
				 	
				 		<a id="setImg" class="label label-info">头像设置</a>
				 		<a href="#"  class="label label-info">用户名更改</a>
				 		<a href="#" class="label label-info">充值</a>
				 		<a id="edit_info" href="#" class="label label-info">个人信息更改</a>
				 		<a class="label label-info" href="<?php echo U('/Home/User/Logout');?>">退出</a>
			
					
				</div>	
		</div>

		<br/>
				
				<!-- 内容切换区域 -->
				<ul id="list">
					<!-- 修改头像开始 -->
					<li id="imgArea" style="display:none" style=" 
						        <?php if(($setImgDisplay == 'show')): ?>'display':'block'<?php endif; ?>">
					
						<div  class="col-md-8 col-xs-8 col-sm-offset-3">
							<form role="form" action="<?php echo U('/Home/User/setImg');?>" method="post" enctype="multipart/form-data">			 
							  <div class="form-group">
							    <label for="inputfile">文件输入</label>
							    <input type="file" id="inputfile" name="headImg">
								
							  </div>
							  <input type="hidden" value="<?php echo ($userInfo["id"]); ?>" name="userDir">
							  <button type="submit" class="btn btn-default" id="headImgBtn">提交</button>
							</form>

						</div>
					</li>
					<!-- 修改头像结束 -->


					<li id="user_info" style="display:none" class=" 
						        <?php if(($display == 'show')): ?>display:'block'<?php endif; ?>">
						<!-- 个人信息查看修改 -->
						<div >
							<form action="<?php echo U('/Home/User/Edit_info');?>" method="post">

											<input type="hidden" name="user_id" value="<?php echo ($userInfo["id"]); ?>" />
										   <div class="row col-sm-offset-3 ">
											 	<div class="col-md-7 col-xs-7">
													<span class="label label-default">用户名：</span>
													<input class="form-control col-md-6 col-xs-6" type="text" name="user_name" value="<?php echo ($userInfo["username"]); ?>" disabled="disabled">
												</div>	
											</div>
											<br/>
											<div class="row col-sm-offset-3 ">
												<div class="col-md-7 col-xs-7">
													<span class="label label-default">手机：</span>
													<input  class="form-control col-md-6 col-xs-6" type="text" name="user_phone" value="<?php echo ($userInfo["phone"]); ?>" >
												</div>
											</div>
											<br/>
											<div class="row col-sm-offset-3 ">
												<div class="col-md-7 col-xs-7">
													<span class="label label-default">邮箱：</span>
													<input  class="form-control col-md-6 col-xs-6" type="text" name="user_email" value="<?php echo ($userInfo["email"]); ?>" >
												</div>
											</div>
											<br/>
											<div class="row col-sm-offset-3 ">
												<div class="col-md-7 col-xs-7">

													<span class="label label-default">性别：</span>
													<label class="radio-inline">
													    <input type="radio" style="width:20px;height:20px" name="user_sex" value="0" <?php if(($userInfo["sex"] == 0)): ?>checked<?php endif; ?>>&nbsp; 男				           
													</label>
													

													<label class="radio-inline">
													  <input type="radio" style="width:20px;height:20px" name="user_sex" value="1" <?php if(($userInfo["sex"] == 1)): ?>checked<?php endif; ?>>&nbsp;女 
													</label>								  
						    </div>
						</div>	
						
						<br/>
						<div class="row col-sm-offset-4 ">
							<input type="submit" class="btn btn-primary col-md-4 col-xs-4" value="保存">
						</div>
						

						</form>
						<!-- 个人信息查看修改结束 -->
			</li>
		
		</ul>
	  <!-- 内容切换区域结束 -->
	</div>	
</div>
	
	
</body>
</html>