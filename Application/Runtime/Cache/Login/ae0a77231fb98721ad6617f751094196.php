<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="/www/SaySaying/Public/css/bootstrap.min.css"/>
    <script src="/www/SaySaying/Public/js/jquery-1.12.3.js" type="text/javascript" charset="utf-8"></script>
    <script src="/www/SaySaying/Public/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
<div class="container">
    <br /><br /><br />
    <br /><br />
    <div class="text-center">
        <img src="/www/SaySaying/Public/img/logo.png"/>
        <span class="text-info" style="font-size: 30px;display: inherit;">SaySaying</span>
    </div>
    <br />
    <form class="" method="post" action="<?php echo U('/Login/Index/index') ?>">
        <div>
            <label for="exampleInputName2">账号 :</label>
            <input type="text" class="form-control" name="userid" id="exampleInputName2" placeholder="用户名 / 邮箱 / 手机">
        </div><br />
        <div>
            <label for="exampleInputEmail2">密码 :</label>
            <input type="password" class="form-control" name="pass" id="exampleInputEmail2" placeholder="密码">
        </div>
        <br />
        <button type="submit" class="btn btn-block btn-success">登录</button><br />
        <a href="<?php echo U('/Login/Register/index') ?>"><button type="button" class="btn btn-block btn-danger">立即注册</button></a>
    </form>
</div>
</body>
</html>