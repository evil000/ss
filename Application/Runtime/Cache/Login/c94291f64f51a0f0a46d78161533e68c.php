<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="/www/SaySaying/Public/css/bootstrap.min.css">
	<style type="text/css">
	.container{
		margin-top: 4em;
	}
	</style>
</head>
<body>
<div class="container">
	<div class="top navbar-fixed-top text-center">
		<div class="logo">英文logo 用户注册</div>
	</div>
	<form class="form-horizontal" method="post" action="<?php echo U('Login/Register/index?do=chk') ?>">
	  <div class="form-group">
	    <label for="" class="col-sm-2 col-xs-2 control-label">用户名</label>
	    <div class="col-sm-10 col-xs-10">
	      <input type="email" class="form-control" name="user" id="" placeholder="用户名">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="" class="col-sm-2 col-xs-2 control-label">账号</label>
	    <div class="col-sm-10 col-xs-10">
	      <input type="email" class="form-control" name="userid" id="" placeholder="账号">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="" class="col-sm-2 col-xs-2 control-label">密码</label>
	    <div class="col-sm-10 col-xs-10">
	      <input type="password" class="form-control" name="pass" id="" placeholder="密码">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="" class="col-sm-2 col-xs-2 control-label">确认密码</label>
	    <div class="col-sm-10 col-xs-10">
	      <input type="password" class="form-control"  id="" placeholder="确认密码">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="" class="col-sm-2 col-xs-2 control-label">邮箱</label>
	    <div class="col-sm-10 col-xs-10">
	      <input type="password" class="form-control" name="email" id="" placeholder="邮箱">
	    </div>
	  </div>
	   <div class="form-group">
	    <label for="" class="col-sm-2 col-xs-2 control-label">手机</label>
	    <div class="col-sm-10 col-xs-10">
	      <input type="password" class="form-control" name="phone" id="" placeholder="手机">
	    </div>
	  </div>
	  <div class="form-group row">
	  	 <label for="" class="col-sm-12 col-xs-12 control-label" name="school" style="text-align: left;">选择你所在的学校:</label>
	  	 <div class="col-sm-12 col-xs-12">
	  	 	<div class="col-sm-4 col-xs-4">
		    	<select style="width: 100%;" class="center-block text-center">
		    	<option>省</option></select>
		    </div>
		    <div class="col-sm-4 col-xs-4">
		    	<select style="width: 100%;" class="center-block text-center">
		    	<option>市</option></select>
		    </div>
		    <div class="col-sm-4 col-xs-4">
		    	<select style="width: 100%;" class="center-block text-center">
		    	<option>县</option></select>
		    </div>
		   	<div class="col-sm-12 col-xs-12" style="margin-top: 1em;">
		   		<select style="width: 80%;" class="center-block text-center">
		   		<option>学校</option></select>
		   	</div>
	    </div>
	  </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">注册</button>
            </div>
        </div>
	</form>
</div>
</body>
</html>