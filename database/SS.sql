﻿DROP TABLE IF EXISTS `say_auth_group_access`;

#用户表

#三张管理员表
 DROP TABLE IF EXISTS `say_auth_rule`;
CREATE TABLE `say_auth_rule` (  
    `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,  
    `name` char(80) NOT NULL DEFAULT '',  
    `title` char(20) NOT NULL DEFAULT '',  
    `type` tinyint(1) NOT NULL DEFAULT '1',    
    `status` tinyint(1) NOT NULL DEFAULT '1',  
    `condition` char(100) NOT NULL DEFAULT '',  # 规则附件条件,满足附加条件的规则,才认为是有效的规则
    PRIMARY KEY (`id`),  
    UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
-- ----------------------------
-- think_auth_group 用户组表， 
-- id：主键， title:用户组中文名称， rules：用户组拥有的规则id， 多个规则","隔开，status 状态：为1正常，为0禁用
-- ----------------------------
 DROP TABLE IF EXISTS `say_auth_group`;
CREATE TABLE `say_auth_group` ( 
    `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT, 
    `title` char(100) NOT NULL DEFAULT '', 
    `status` tinyint(1) NOT NULL DEFAULT '1', 
    `rules` char(80) NOT NULL DEFAULT '', 
    PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
-- ----------------------------
-- think_auth_group_access 用户组明细表
-- uid:用户id，group_id：用户组id
-- ----------------------------
DROP TABLE IF EXISTS `say_group_access`;
CREATE TABLE `say_auth_group_access` (  
    `uid` mediumint(8) unsigned NOT NULL,  
    `group_id` mediumint(8) unsigned NOT NULL, 
    UNIQUE KEY `uid_group_id` (`uid`,`group_id`),  
    KEY `uid` (`uid`), 
    KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
#评论表
create table say_comment(
		id  int  key  auto_increment,  #评论id
		wid int  unsigned not null, #谁评论的  
		did  int unsigned not null, #给谁评论的
		content  varchar(120)  not null, #评论内容 
		datetime  date   not null,#评论时间
		status int default 0#状态 0不显示 1显示 
);
#表白内容

#省表
create table say_province 
(
	id int  key auto_increment,
	pname  varchar(30) not null    #省名称
);
#市表
create table say_city
(
	id int key auto_increment,
	pid int not null,#外键=>省表
	cname varchar(50)  not null  #市名称
);
#区/县表
create table say_area 
(
	id int  key auto_increment,
	pid int not null,#外键=>省表
	cid int not null,#外键=>市表
	aname  varchar(50)  not null #区名称
);


create table say_user 
(
	id  int key auto_increment,   #主键
	username varchar(24)  unique not null, #用户名 唯一
	password char(32) not null,  #密码
	phone char(11) unique not null, #手机号  唯一
	email varchar(50)  unique not null,#邮箱 唯一
	ssid int not null,#外键=>学校表
	sex int  not null default 0 #性别 0=》保密 1=>女 2=》男
	
);

#学校表(市下学校，区/县下学校)本来想拆分的 发现麻烦
create table say_pro_city_school
(
	id  int  key auto_increment,
	pid  int not null,#外键=>省表
	cid  int not null,#外键=>市表
	aid int default 0,#外键=>区表  我这里给它一个默认是因为 可能到市 学校了  就不到区/县了
	sname varchar(100) not null  #学校名称
	
);